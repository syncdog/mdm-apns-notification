﻿namespace SendNotification
{
    public class Attribute
    {
        public long AttributeId { get; set; }
        public string FkReference { get; set; }
        public long FkId { get; set; }
        public string AttributeKey { get; set; }
        public string AttributeValue { get; set; }
    }
}
