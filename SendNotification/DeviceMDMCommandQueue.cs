﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SendNotification
{
    public class DeviceMDMCommandQueue
    {
        [Key]
        public long DeviceMDMCommandID {get;set;}
      public string CommandUUID {get;set;}
      public string ContentType {get;set;}
      public string Command {get;set;}
      public bool? SignPayload {get;set;}
      public DateTime? DateCreated {get;set;}
      public DateTime? DeviceConfirmationDate {get;set;}
      public long DeviceID {get;set;}
      public string DeviceResponse {get;set;}
      public string DeviceUDID {get;set;}
      public DateTime? DateModified {get;set;}
      public DateTime? ScheduledRetry { get; set; }
      public int? RetryCount { get; set; }

        public DateTime? DeviceNotificationDate { get; set; }
    }
}
