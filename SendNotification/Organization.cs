﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SendNotification
{
    public class Organization
    {
        public long OrganizationId {get;set;}
      public string OrganizationName  {get;set;}
      public long? ParentOrganizationId {get;set;}
      public int? Level {get;set;}
      public char Active {get;set;}
      public DateTime? DateCreated {get;set;}
      public DateTime? DateModified {get;set;}
      public char? SelfHelpEnabled {get;set;}
      public bool? AndroidWorkEnabled {get;set;}
      public string SignupUrlName {get;set;}
      public string EnterpriseToken {get;set;}
      public string DeveloperAccountId {get;set;}
      public string DeveloperAccountLink {get;set;}
      public string AndroidCompletionToken {get;set;}
      public bool? AndroidEMMEnabled {get;set;}

    }
}
