﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using dotAPNS;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using Org.BouncyCastle.OpenSsl;
using System.Security;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Parameters;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Runtime.Caching;
using System.Threading;
using NLog;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace SendNotification
{
    class Program
    {
        private static readonly ObjectCache APNS_CERTIFICATE_CACHE = new MemoryCache("APNS_CERTIFICATES");
        private static readonly ObjectCache APNS_CERTIFICATE_ID_CACHE = new MemoryCache("APNS_ID_CERTIFICATES");
        private const int DEFAULT_SLEEP_INTERVAL = 120000;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static string ConStr = String.Empty;
        private const int keysize = 256;
        private static string InitVector = "tu89geji340t89u2";


        public static void PurgeApnsCache()
        {
            List<string> cacheKeys = APNS_CERTIFICATE_CACHE.Select(kvp => kvp.Key).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                APNS_CERTIFICATE_CACHE.Remove(cacheKey);
            }
            List<string> cacheIdKeys = APNS_CERTIFICATE_ID_CACHE.Select(kvp => kvp.Key).ToList();
            foreach (string id in cacheIdKeys)
            {
                APNS_CERTIFICATE_ID_CACHE.Remove(id);
            }
        }


        public static string GetDBConnectionString()
        {
            string pswd = "LNvHPmU5FdmDyhnHEeGr2MxPxRWW9w49dtG3HKXEQ8wfhRmCZLYbhVWeuXK6svqC";

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            IConfiguration config = builder.Build();

            var conStr = config.GetSection("ConnectionStrings").Get<Configuration>();

            var decrypted = Decrypt(conStr.DefaultConnection, pswd);


            return decrypted;
        }

        public static int GetSleepInterval()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            IConfiguration config = builder.Build();

            int sleep = config.GetSection("SleepInterval").Get<int>();
            if (sleep > 0)
            {
                return sleep;
            }

            return DEFAULT_SLEEP_INTERVAL;
        }

        static async Task  Main(string[] args)
        {

            Logger.Info("Start up...");

            int sleepInterval = GetSleepInterval();


            ConStr = GetDBConnectionString();

            while (true)
            {
                await using (var context = new SentinelSecureContext(ConStr))
                {
                    var deviceList = GetIosDeviceList(context);

                    foreach (var deviceId in deviceList)
                    {
                        var oldestCommand = GetMdmCommandList(context, deviceId)
                            .OrderBy(c => c.DateCreated)
                            .FirstOrDefault();

                        if (oldestCommand != null)
                        {
                            await NotifyDevice(context, oldestCommand);
                        }

                    }
                }
                Logger.Debug("Sleeping for " + sleepInterval + " ms");
                Thread.Sleep(sleepInterval);
                Logger.Debug("Waking up...");
            }
        }

        private static async Task NotifyDevice(SentinelSecureContext context, DeviceMDMCommandQueue item)
        {
            var result = await ProcessTheCommand(context, item);

            if (result.Success)
            {
                item.DeviceNotificationDate = DateTime.UtcNow;
                context.SaveChanges();

                Logger.Info("Push sent successfully for DeviceID = {0} CommandUUID={1}", item.DeviceID, item.CommandUUID);
            }
            else
            {
                bool updateCommand = true;
                item.DeviceNotificationDate = DateTime.Now;
                item.DeviceConfirmationDate = DateTime.Now;
                switch (result.ResponseCode)
                {
                    case -999:
                        item.DeviceResponse = "Skipping stale command";
                        break;
                    case -888:
                        updateCommand = false;
                        break;
                    case 400:
                        item.DeviceResponse = result.FailureReason;
                        break;
                    case 403:
                        item.DeviceResponse =
                            "There was an error with the certificate or with the provider’s authentication token. [403]";
                        break;
                    case 404:
                        item.DeviceResponse = "The request contained an invalid :path value. [404]";
                        break;
                    case 405:
                        item.DeviceResponse =
                            "The request used an invalid :method value. Only POST requests are supported. [405]";
                        break;
                    case 410:
                        item.DeviceResponse = "The device token is no longer active for the topic. [410]";
                        break;
                    case 413:
                        item.DeviceResponse = "The notification payload was too large. [413]";
                        break;
                    case 429:
                        item.DeviceResponse = "The server received too many requests for the same device token. [429]";
                        break;
                    case 500:
                        item.DeviceResponse = "Internal server error. [500]";
                        break;
                    case 503:
                        item.DeviceResponse = "The server is shutting down and unavailable. [503]";
                        break;
                }

                if (updateCommand)
                {
                    context.SaveChanges();
                }
            }
        }

        /**
         * If the command is over 24 hours old, then finally fail it.
         */
        private static bool IsStaleCommand(DeviceMDMCommandQueue item)
        {
            if (!item.DateCreated.HasValue)
            {
                return true;
            }

            if (!item.ScheduledRetry.HasValue)
            {
                return false;
            }
            var dateCreated = item.DateCreated.Value;
            var now = DateTime.Now;

            return now.AddDays(-1).CompareTo(dateCreated) > 0;
        }

        private static IList<DeviceMDMCommandQueue> GetNewMDMCommandList(SentinelSecureContext context)
        {
            IList<DeviceMDMCommandQueue> list = null;

            var iosDeviceList = GetIosDeviceList(context);

            list = context.DeviceMDMCommandQueue
                .Where(dcmd => !dcmd.DeviceNotificationDate.HasValue && iosDeviceList.Contains(dcmd.DeviceID))
                .ToList();

            return list;

        }

        private static IList<DeviceMDMCommandQueue> GetMdmCommandList(SentinelSecureContext context, long deviceId)
        {
            IList<DeviceMDMCommandQueue> list = context.DeviceMDMCommandQueue
                .Where(dcmd => dcmd.DeviceID == deviceId)
                .Where(dcmd => !dcmd.DeviceNotificationDate.HasValue || dcmd.ScheduledRetry.HasValue)
                .OrderBy(dcmd => dcmd.DateCreated)
                .ToList();

            DbFunctions dfunc = null;
            IList<DeviceMDMCommandQueue> oldCommands = context.DeviceMDMCommandQueue
                .Where(dcmd => dcmd.DeviceID == deviceId)
                .Where(dcmd => dcmd.DeviceNotificationDate.HasValue)
                .Where(dcmd => !dcmd.DeviceConfirmationDate.HasValue)
                .Where(dcmd => dcmd.DateCreated.HasValue && dfunc.DateDiffHour(dcmd.DateCreated, DateTime.Now) <= 4)
                .OrderBy(dcmd => dcmd.DateCreated)
                .ToList();

            return oldCommands.Concat(list).ToList();

        }

        private static List<long> GetIosDeviceList(SentinelSecureContext context)
        {
            return context.Device
                .Where(d => d.Active.HasValue && d.Active == 'Y')
                .Where(d => "ios".Equals(d.OS.ToLower()))
                .Where(device => device.IsMdmDevice ?? false)
                .Where(device => !string.IsNullOrEmpty(device.PushToken))
                .Select(d => d.DeviceID)
                .ToList();
        }

        private static readonly Regex UdidRegex = new Regex("(?<udid>[A-Za-z0-9]+)(\\.(?<username>.*))?");

        private static async Task<ApnsResult> ProcessTheCommand(SentinelSecureContext context, DeviceMDMCommandQueue item)
        {
            ApnsResult result = new ApnsResult
            {
                Success = false
            };

            if (IsStaleCommand(item))
            {
                result.ResponseCode = -999;
                result.FailureReason = "Stale Command, invalidating";
                return result;
            }

            if (item.ScheduledRetry.HasValue && DateTime.Compare(DateTime.Now, item.ScheduledRetry.Value) < 0)
            {
                result.ResponseCode = -888;
                result.FailureReason = "Not Yet Scheduled for retry";
                return result;
            } 

            var device = context.Device.FirstOrDefault(d => d.DeviceID == item.DeviceID);

            if (device == null)
            {
                Logger.Error("Device is null for DeviceID = {0}", item.DeviceID);
                result.FailureReason = string.Format("DeviceID={0} not found", item.DeviceID);
                // device was most likely deleted just acknowledge the command so we don't try again
                return result;
            }

            if (!"ios".Equals(device.OS, StringComparison.CurrentCultureIgnoreCase))
            {
                Logger.Error("Device is not iOS for DeviceID = {0}", item.DeviceID);
                result.FailureReason = string.Format("Device is not iOS for DeviceID = {0}", item.DeviceID);
                // device is wrong type, ack so it doesn't get re-processed
                return result;
            }

            if (string.IsNullOrEmpty(device.PushToken))
            {
                Logger.Error("Device empty PushToken for DeviceID = {0}", item.DeviceID);
                // device has empty push token
                result.FailureReason = string.Format("Device empty PushToken for DeviceID = {0}", item.DeviceID);
                return result;
            }

            var user = context.Users.FirstOrDefault(u => u.UserID == device.UserID);

            if (user == null)
            {
                Logger.Error("User is null for UserID = {0}", device.UserID);
                // device was most likely deleted just acknowledge the command so we don't try again
                result.FailureReason = string.Format("User is null for UserID = {0}", device.UserID);
                return result;
            }


            var cacheKey = "ORGID_" + user.OrganizationId;

            var clientCertificate = FindApnsCertificate(context, cacheKey, user);

            if (clientCertificate != null)
            {

                var pushToken = device.PushToken.TrimEnd('\r', '\n', '\t');
                pushToken = pushToken.TrimStart('\r', '\n', '\t');

                var pushMagic = device.PushMagic;
                
                var udid = item.DeviceUDID;
                var match = UdidRegex.Match(udid);
                if (match.Success)
                {
                    var isUserChannel = match.Groups.ContainsKey("username") && !string.IsNullOrEmpty(match.Groups["username"].Value);
                    if (isUserChannel)
                    {
                        var userName = match.Groups["username"].Value;
                        var attributes = context.Attributes.Where(a =>
                            "DEVICE".Equals(a.FkReference) && a.FkId == device.DeviceID &&
                            "MacOSUser".Equals(a.AttributeKey));
                        foreach (var userAttr in attributes)
                        {
                            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(userAttr.AttributeValue);
                            if (dictionary != null 
                                && dictionary.ContainsKey("userLongName") 
                                && dictionary["userLongName"].Equals(userName) 
                                && dictionary.ContainsKey("pushToken") 
                                && dictionary.ContainsKey("pushMagic"))
                            {
                                pushToken = dictionary["pushToken"];
                                pushMagic = dictionary["pushMagic"];
                                break;
                            }
                        }
                    }
                }

                byte[] data = Convert.FromBase64String(pushToken);
                string hex = BitConverter.ToString(data).Replace("-", string.Empty);

                try
                {
                    result = await SendAPNS(item.CommandUUID, clientCertificate, hex, device.Topic, pushMagic,
                        device.DeviceID);
                    if (!result.Success)
                    {
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, string.Format("Error attempting to send APNS for commandUUID={0}, deviceId={1}", item.CommandUUID, device.DeviceID));
                    result.FailureReason = string.Format("Error attempting to send APNS for commandUUID={0}, deviceId={1}", item.CommandUUID, device.DeviceID);
                    return result;
                }

                result.Success = true;
                return result;
            }

            Logger.Error("Push Certificate not found for OrgID = {0}",user.OrganizationId);
            result.FailureReason = string.Format("Push Certificate not found for OrgID = {0}", user.OrganizationId);
            return result;


        }

        private static X509Certificate2 FindApnsCertificate(SentinelSecureContext context, string cacheKey, User user)
        {
            X509Certificate2 clientCertificate = APNS_CERTIFICATE_CACHE.Get(cacheKey) as X509Certificate2;
            if (clientCertificate != null && DateTime.Now.CompareTo(clientCertificate.NotAfter) > 0)
            {
                clientCertificate = null;
            }
            var orgId = user.OrganizationId ?? 0;

            if (clientCertificate == null)
            {
                var ancestry = GetAncestry(orgId);

                foreach (var org in ancestry)
                {
                    var mdmCertificate = context.MDMCertificateStore.FirstOrDefault(c =>
                        c.FkReference.Equals("ORGANIZATION") && c.FkId == org.OrganizationId && c.UsageType.Equals("APNS") &&
                        c.Platform.Equals("APPLE") && c.Active);

                    if (mdmCertificate != null)
                    {
                        var pemReader = new PemReader(new StringReader(mdmCertificate.Certificate));

                        var x509 = (Org.BouncyCastle.X509.X509Certificate) pemReader.ReadObject();
                        var cert = new X509Certificate2(x509.GetEncoded(), new SecureString(),
                            X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet |
                            X509KeyStorageFlags.PersistKeySet);

                        var privateKey = context.MDMCertificateStore.FirstOrDefault(c =>
                            c.FkReference.Equals("ORGANIZATION") && c.FkId == org.OrganizationId &&
                            c.UsageType.Equals("APNS_PRIVATE_KEY") && c.Platform.Equals("APPLE") && c.Active);

                        if (privateKey != null)
                        {
                            try
                            {
                                pemReader = new PemReader(new StringReader(privateKey.Certificate));
                                var pk = (AsymmetricCipherKeyPair)pemReader.ReadObject();

                                RSACryptoServiceProvider.UseMachineKeyStore = true;
                                var csp = new CspParameters
                                {
                                    ProviderType = 1,
                                    Flags = CspProviderFlags.UseMachineKeyStore,
                                    KeyContainerName = Guid.NewGuid().ToString().ToUpperInvariant()
                                };

                                var rsaPriv = DotNetUtilities.ToRSA(pk.Private as RsaPrivateCrtKeyParameters, csp);

                                var rsaPrivate = new RSACryptoServiceProvider(csp);
                                rsaPrivate.ImportParameters(rsaPriv.ExportParameters(true));


                                //  cert.PrivateKey = rsaPrivate;

                                clientCertificate = cert.CopyWithPrivateKey(rsaPriv);

                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, "Error processing APNS certificate orgId=" + orgId + ", certificateId=" + mdmCertificate.CertificateId + ", privateKeyId=" + privateKey.CertificateId);
                                return null;
                            }

                            CacheItemPolicy policy = new CacheItemPolicy
                            {
                                AbsoluteExpiration = DateTimeOffset.Now.AddDays(1)
                            };

                            Logger.Info("Using APNS certificateId={0} [orgId={1},userId={2}]", mdmCertificate.CertificateId, orgId, user.UserID);

                            APNS_CERTIFICATE_CACHE.Set(cacheKey, clientCertificate, policy);
                            APNS_CERTIFICATE_ID_CACHE.Set(cacheKey, mdmCertificate.CertificateId.ToString(), policy);
                            return clientCertificate;
                        }
                    }
                }
            }

            string id = APNS_CERTIFICATE_ID_CACHE.Get(cacheKey) as string;
            Logger.Info("Using APNS certificateId={0} [orgId={1},userId={2}]", id, orgId, user.UserID);

            return clientCertificate;
        }

        class ApnsResult
        {
            public bool Success { get; set; }
            public string FailureReason { get; set; }
            public int ResponseCode { get; set; }
        }

        private static async Task<ApnsResult> SendAPNS(string commandUUID, X509Certificate2 theCert, string pushToken, string topic, string pushMagic, long deviceId)
        {
            var result = new ApnsResult();

            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;

            handler.ClientCertificates.Add(theCert);
            var client = new HttpClient(handler);

            string url = "https://api.push.apple.com:443/3/device/" + pushToken;



            var req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Version = new Version(2, 0);
            req.Headers.Add("apns-priority", "10");
            req.Headers.Add("apns-push-type", "mdm");
            req.Headers.Add("apns-topic", topic);
            req.Content = new JsonContent(String.Format("{{\"mdm\":\"{0}\"}}",pushMagic));

            using (var response = await client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead))
            {
                // HttpStatusCode statusCode = response.StatusCode;
                string reasonPhrase = response.ReasonPhrase;
                result.Success = response.IsSuccessStatusCode;
                if (!result.Success)
                {
                    result.ResponseCode = (int)response.StatusCode;
                    result.FailureReason = reasonPhrase;

                    var task = response.Content.ReadAsStringAsync();
                    var json = Task.Run(() => task).Result;
                    if (!string.IsNullOrEmpty(json))
                    {
                        result.FailureReason += " [" + json + "]";
                    }
                    Logger.Error("Error send push commandUUID = {0}, DeviceID = {1}, PushToken = {2}, PushMagic = {3}, Error = {4}, ErrorCode = {5}, Response = {6}", 
                        commandUUID, deviceId, pushToken, pushMagic, reasonPhrase, (int) response.StatusCode, result.FailureReason);
                }
            }

            return result;
        }


        private static IList<Organization> GetAncestry(long organizationId)
        {
            IList<Organization> list = null;

            string sql = "WITH #results " +
                                "AS ( " +
                                "	SELECT organizationid " +
                                "		,organizationName " +
                                "		,parentorganizationid " +
                                "		,Active " +
                                "		,DateCreated " +
                                "		,DateModified " +
                                "      ,SelfHelpEnabled " +
                                  ",EnterpriseToken " +
                                ",SignupUrlName " +
                                ",AndroidWorkEnabled " +
                                 ",DeveloperAccountId " +
                                ",DeveloperAccountLink" +
                                 ",AndroidCompletionToken " +
                                 ",AndroidEMMEnabled " +
                                "		,1 AS LEVEL " +
                                "	FROM Organization " +
                                "	WHERE organizationid = " + organizationId +
                                "	UNION ALL " +
                                "	SELECT o.organizationid " +
                                "		,o.organizationName " +
                                "		,o.parentorganizationid " +
                                "		,o.Active " +
                                "		,o.DateCreated " +
                                "		,o.DateModified " +
                                "      ,o.SelfHelpEnabled " +
                                  ",o.EnterpriseToken " +
                                ",o.SignupUrlName " +
                                ",o.AndroidWorkEnabled " +
                                 ",o.DeveloperAccountId " +
                                ",o.DeveloperAccountLink" +
                                 ",o.AndroidCompletionToken " +
                                 ",o.AndroidEMMEnabled " +
                                "		,ol.LEVEL + 1 " +
                                "	FROM Organization o " +
                                "	INNER JOIN #results ol ON ol.parentorganizationid = o.organizationid " +
                                "	) " +
                                "SELECT OrganizationId " +
                                ",OrganizationName " +
                                ",ParentOrganizationId " +
                                ",cast(active as CHAR) as Active " +
                                ",DateCreated " +
                                ",DateModified " +
                                ",SelfHelpEnabled " +
                                  ",EnterpriseToken " +
                                ",SignupUrlName " +
                                ",AndroidWorkEnabled " +
                                 ",DeveloperAccountId " +
                                ",DeveloperAccountLink" +
                                 ",AndroidCompletionToken " +
                                 ",AndroidEMMEnabled " +
                                ",Level " +
                                "FROM #results " +
                                "order by level";

            using (var context = new SentinelSecureContext(ConStr))
            {
                list = context.Organization.FromSqlRaw(sql).ToList();
            }


            return list;
        }

        private static string Decrypt(string cipherText, string pswd)
        {
            if (!String.IsNullOrEmpty(cipherText))
            {
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
                byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
                var password = new PasswordDeriveBytes(pswd, null);
                byte[] keyBytes = password.GetBytes(keysize / 8);
                var symmetricKey = new RijndaelManaged();
                symmetricKey.Mode = CipherMode.CBC;
                ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
                var memoryStream = new MemoryStream(cipherTextBytes);
                var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                var plainTextBytes = new byte[cipherTextBytes.Length];
                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                memoryStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            }
            else
            {
                return String.Empty;
            }
        }
    }
}
