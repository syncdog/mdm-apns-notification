﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SendNotification
{
    public class User
    {
        public long UserID {get;set;}
      public string UserName {get;set;}
      public long? OrganizationId {get;set;}
      public string EmailAddress {get;set;}
      public string PIN {get;set;}
      public long? UserStatusID {get;set;}
      public string FirstName {get;set;}
      public string LastName {get;set;}
      public DateTime? DateCreated {get;set;}
      public DateTime? DateModified {get;set;}
      public string Host {get;set;}
      public string AuthToken {get;set;}
      public long? TaskServerId {get;set;}
      public char? Active {get;set;}
      public char? NotificationFrequency {get;set;}
      public char? NotificationStyle {get;set;}
      public string PhoneMobile {get;set;}
      public string PhoneOther {get;set;}
      public string EmailOther {get;set;}
      public long? RelayID {get;set;}
      public DateTime? DateDeleted {get;set;}
      public string NetworkUserName {get;set;}
      public string Domain {get;set;}
      public string EnrollmentTokenName {get;set;}
      public string OrganizationName {get;set;}

    }
}
