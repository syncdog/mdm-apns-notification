﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SendNotification
{
    public class Device
    {

       public long DeviceID { get; set; }
       public string ClientUniqueID  { get; set; }
      public string DeviceName  { get; set; }
      public long? UserID  { get; set; }
      public DateTime? DateCreated  { get; set; }
       public char? Active  { get; set; }
      public string DeviceTypeID { get; set; }
      public string CertificateName  { get; set; }
      public string Model { get; set; }
      public string OS { get; set; }
      public string OSVersion { get; set; }
      public DateTime? LastConnectTime { get; set; }
      public DateTime? LastNotificationTime { get; set; }
      public DateTime? ProvisionDate { get; set; }
      public bool? APNSDebug { get; set; }
      public DateTime? LastPing { get; set; }
      public long? TaskServerId { get; set; }
      public string LastSyncKey { get; set; }
      public DateTime? DateModified { get; set; }
      public string PushToken { get; set; }
      public string TemporaryResetPassword { get; set; }
        public string UnlockPIN { get; set; }
        public byte[] Certificate { get; set; }
        public string PrivateKey { get; set; }
        public string Carrier { get; set; }
      public DateTime? DateDeleted { get; set; }
      public char? Deleted { get; set; }
      public string Build { get; set; }
      public bool? KioskModeDisabled { get; set; }
      public bool? IsMdmDevice { get; set; }
      public string DeviceConfiguration { get; set; }
      public string DeviceDetails { get; set; }
      public string Topic { get; set; }
      public string SerialNumber { get; set; }
      public string PushMagic { get; set; }
      public string UnlockToken { get; set; }
      public bool? IsAppStoreDevice { get; set; }
      public string AppStoreToken { get; set; }
      public int? SEPStatus { get; set; }
      public int? ComplianceStatus { get; set; }



    }
}
