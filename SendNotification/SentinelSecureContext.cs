﻿using Microsoft.EntityFrameworkCore;
using System;

namespace SendNotification
{
    public class SentinelSecureContext : DbContext
    {
        private string DBConnectionString { get; set; }

        public SentinelSecureContext(string conStr)
        {
            DBConnectionString = conStr;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=SentinelSecure;User Id=sa;Password=SyncDog1;");
            //  optionsBuilder.UseSqlServer(@"Server=tcp:qasentinelsecure.database.windows.net,1433;Initial Catalog=SentinelSecure;Persist Security Info = False; User ID = ssuser; Password = SSqapwd#1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            optionsBuilder.UseSqlServer(DBConnectionString);
        }

        public DbSet<MDMCertificateStore> MDMCertificateStore { get; set; }
        public DbSet<Device> Device { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Organization> Organization { get; set; }

        public DbSet<DeviceMDMCommandQueue> DeviceMDMCommandQueue { get; set; }
        public DbSet<Attribute> Attributes { get; set; }
    
    }

}

    