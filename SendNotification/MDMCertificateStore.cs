﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SendNotification
{
   public class MDMCertificateStore
    {
        [Key]
        public Guid CertificateId { get; set; }
      public Guid ParentId { get; set; }
      public long FkId { get; set; }
      public string FkReference { get; set; }
      public string Description { get; set; }
      public string Filename { get; set; }
      public string Certificate { get; set; }
      public string Password { get; set; }
      public string DisplayName { get; set; }
      public string Type { get; set; }
      public DateTime? Expiration { get; set; }
      public bool Active { get; set; }
      public DateTime? DateCreated { get; set; }
      public DateTime? DateModified { get; set; }
      public string UsageType { get; set; }
      public string Platform { get; set; }

    }

}
